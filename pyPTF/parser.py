from gplearn.functions import _function_map
from gplearn._program import _Program
from sympy.printing.printer import Printer, print_function
from sympy import sympify, Add, Mul, Pow


class ProgramPrinter(Printer):
    printmethod = "_program"

    _default_settings = {
    }

    def __init__(self, settings=None):
        Printer.__init__(self, settings)


@print_function(ProgramPrinter)
def print_program(expr, **settings):
    return ProgramPrinter(settings).doprint(expr)


def _many_args(op, args):
        out = f'{op}(' * (len(args) - 1)
        out += f'{args[0]}, {args[1]})'
        for arg in args[2:]:
            out += f', {arg})'
        return out


class AddOp(Add):
    def _program(self, printer):
        args = [printer._print(i) for i in self.args]
        return _many_args('add', args)


class MulOp(Mul):
    def _program(self, printer):
        args = [printer._print(i) for i in self.args]
        return _many_args('mul', args)


class PowOp(Pow):
    def _program(self, printer):
        a, b = [printer._print(i) for i in self.args]
        try:
            b = int(b)
            if b == -1:
                return r"div(%s, %s)" % (1, a)
            args = [a] * b
            return _many_args('mul', args)
        except Exception:
            return r"pow(%s, %s)" % (a, b)


gp_ops = {'Add': AddOp, 'Mul': MulOp, 'Pow': PowOp}


def expr_to_program(expr):
    expr2 = sympify(expr, locals=gp_ops, evaluate=False)
    lisp = print_program(expr2)
    prog_str = (lisp.replace('(', ' ')
                .replace(',', '')
                .replace(')', '')
                .split(' ')
                )
    prog = []
    symbols = []
    for n in prog_str:
        if n in _function_map:
            prog.append(_function_map[n])
            continue
        try:
            n = float(n)
            prog.append(n)
            continue
        except ValueError:
            if n not in symbols:
                symbols.append(n)
            prog.append(symbols.index(n))

    return prog, symbols


class MinProgram(_Program):
    def __init__(self, program, feature_names):
        self.program = program
        self.feature_names = feature_names
        if not self.validate_program():
            raise ValueError('The supplied program is incomplete.')
