.. _citing:

How to cite pyPTF
=================

pyPTF (the library)
-------------------

* Padarian, J. 2018-. pyPTF: Pedotransfer function development using Genetic Programming. http://github.com/spadarian/pyPTF; accessed <today>.


The corresponding BibTeX entry::

    @Misc{pyPTF,
      author = {Padarian J},
      title = {{pyPTF: Pedotransfer function development using Genetic Programming}},
      year = {2018--},
      url = "http://github.com/spadarian/pyPTF",
      note = {accessed <today>}
    }



pyPTF (the paper)
-------------------

* Padarian, J. 2019. pyPTF: A python framework for pedotransfer function development. |ImageLink|_


.. |ImageLink| image:: https://upload.wikimedia.org/wikipedia/commons/2/23/Icons-mini-file_acrobat.gif
.. _ImageLink: https://link.url/

The corresponding BibTeX entry::

    @article{padarian2019pyPTF,
      author = {Padarian J},
      title = {{pyPTF: A python framework for pedotransfer function development}},
      year = {2019}
    }
