.. pyPTF documentation master file, created by
   sphinx-quickstart on Thu Sep 27 21:10:22 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyPTF's documentation!
=================================

.. toctree::
   :maxdepth: 2

   intro
   installation
   reference
   example
   citing
